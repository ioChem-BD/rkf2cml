CC = gcc
CFLAGS += -I/usr/local/include/libmxml4 -g
LDFLAGS += -L/usr/lib -L/usr/local/lib -lm
# Detect the operating system and architecture
UNAME_S := $(shell uname -s)
UNAME_M := $(shell uname -m)

# Common flags for all systems
CFLAGS += -Wall -Wextra

# OS-specific flags
ifeq ($(UNAME_S),Darwin)
    # macOS-specific flags
    ifeq ($(UNAME_M),arm64)
        CFLAGS += -arch arm64
    else
        CFLAGS += -arch x86_64
    endif
    LDFLAGS += -Wl,-force_load,/usr/local/lib/libmxml4.a
    LDFLAGS += -Wl,-dead_strip
else
    # Linux and other systems
    LDFLAGS += -static
    LDFLAGS += -Wl,-Bstatic -lmxml4
    LDFLAGS += -Wl,--gc-sections
endif

# Common flags for all systems (continued)
CFLAGS += -ffunction-sections -fdata-sections

all: clean rkf2cml.o ArrayList.o KFReader.o JumboHelper.o JumboCML.o JumboRkf.o
	$(CC) $(CFLAGS) rkf2cml.o ArrayList.o KFReader.o JumboHelper.o JumboRkf.o JumboCML.o $(LDFLAGS) -o rkf2cml

rkf2cml.o: rkf2cml.c
	$(CC) $(CFLAGS) -c rkf2cml.c	

ArrayList.o: ArrayList.c ArrayList.h
	$(CC) $(CFLAGS) -c ArrayList.c

KFReader.o: KFReader.c KFc.h
	$(CC) $(CFLAGS) -c KFReader.c

JumboHelper.o: JumboHelper.c JumboHelper.h
	$(CC) $(CFLAGS) -c JumboHelper.c

JumboCML.o: JumboCML.c JumboCML.h
	$(CC) $(CFLAGS) -c JumboCML.c

JumboRkf.o: JumboRkf.c JumboRkf.h
	$(CC) $(CFLAGS) -c JumboRkf.c

clean:
	rm -f *.o rkf2cml
