#include "JumboCML.h"
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv)
{
    if (argc != 4)
    {
        printf("ioChem-BD tool to convert SCM software keyed-files (.rkf) into Chemical Markup Language (CML) format.\n\n");
        printf("Usage: ./rkf2cml rkffile1 rkffile2 outfile\n");
        printf("Mandatory parameters:\n");
        printf("    rkffile1: Full path to the ams.rkf file\n");
        printf("    rkffile2: Full path to the adf.rkf file\n");
        printf("    outfile: Path to the generated output file, you must have write permissions on that path\n\n");        
        printf("Example:\n\n");
        printf("    ./rkf2cml /opt/calc1/ams.rkf /opt/calc1/adf.rkf /opt/results/output.cml\n");
        printf("Enclose paths in double quotes if there are whitespaces in paths.\n");
        printf("Read more about it on the rkf2cml project repository in GitLab");
        return 1;
    }

    createCmlFile(argv[2], argv[1], argv[3]);
    return 0;
}