/***************************************************************************
 JumboHelper.c - Helper functions to manage formula generation, text processing
                 and type conversion.
 
 Copyright (C) 2022 by Institute of Chemical Reasearch of Catalonia (ICIQ)
 For more info, contact ioChem-BD software team (contact at iochem-bd . com)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation version 3 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 ICIQ owns the intellectual property right for this file and reserves the
 right to distrbute it under a license other than LGPL
 ****************************************************************************/


#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "JumboCML.h"
#include <ctype.h>



char ELEMENT_TYPES[120][3] = {"X", "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn", "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac", "Th", "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No", "Lr", "Rf", "Db", "Sb", "Bh", "Hs", "Mt", "Ds", "Rg", "Cn", "Nh", "Fl", "Mc", "Lv", "Ts", "Og"};

char ELEMENT_ALPHA[120][3] = {"X", "Ac", "Ag", "Al", "Am", "Ar", "As", "At", "Au", "B", "Ba", "Be", "Bh", "Bi", "Bk", "Br", "C", "Ca", "Cd", "Ce", "Cf", "Cl", "Cm", "Cn", "Co", "Cr", "Cs", "Cu", "Db", "Ds", "Dy", "Er", "Es", "Eu", "F", "Fe", "Fl", "Fm", "Fr", "Ga", "Gd", "Ge", "H", "He", "Hf", "Hg", "Ho", "Hs", "I", "In", "Ir", "K", "Kr", "La", "Li", "Lr", "Lu", "Lv", "Mc", "Md", "Mg", "Mn", "Mo", "Mt", "N", "Na", "Nb", "Nd", "Ne", "Nh", "Ni", "No", "Np", "O", "Og", "Os", "P", "Pa", "Pb", "Pd", "Pm", "Po", "Pr", "Pt", "Pu", "Ra", "Rb", "Re", "Rf", "Rg", "Rh", "Rn", "Ru", "S", "Sb", "Sc", "Se", "Sg", "Si", "Sm", "Sn", "Sr", "Ta", "Tb", "Tc", "Te", "Th", "Ti", "Tl", "Tm", "Ts", "U", "V", "W", "Xe", "Y", "Yb", "Zn", "Zr"};

char SOLVENT_NAME[48][25] = {"AceticAcid", "Acetone", "Acetonitrile", "Ammonia", "Aniline", "Benzene", "BenzylAlcohol", "Bromoform", "Butanol", "isoButanol", "tertButanol", "CarbonDisulfide", "CarbonTetrachloride", "Chloroform", "Cyclohexane", "Cyclohexanone", "Dichlorobenzene", "DiethylEther", "Dioxane", "DMFA", "DMSO", "Ethanol", "EthylAcetate", "Dichloroethane", "EthyleneGlycol", "Formamide", "FormicAcid", "Glycerol", "HexamethylPhosphoramide", "Hexane", "Hydrazine", "Methanol", "MethylEthylKetone", "Dichloromethane", "Methylformamide", "Methypyrrolidinone", "Nitrobenzene", "Nitrogen", "Nitromethane", "PhosphorylChloride", "IsoPropanol", "Pyridine", "Sulfolane", "Tetrahydrofuran", "Toluene", "Triethylamine", "TrifluoroaceticAcid", "Water"};
char SOLVENT_NAME_UPPERCASE[48][25] = {"ACETICACID", "ACETONE", "ACETONITRILE", "AMMONIA", "ANILINE", "BENZENE", "BENZYLALCOHOL", "BROMOFORM", "BUTANOL", "ISOBUTANOL", "TERTBUTANOL", "CARBONDISULFIDE", "CARBONTETRACHLORIDE", "CHLOROFORM", "CYCLOHEXANE", "CYCLOHEXANONE", "DICHLOROBENZENE", "DIETHYLETHER", "DIOXANE", "DMFA", "DMSO", "ETHANOL", "ETHYLACETATE", "DICHLOROETHANE", "ETHYLENEGLYCOL", "FORMAMIDE", "FORMICACID", "GLYCEROL", "HEXAMETHYLPHOSPHORAMIDE", "HEXANE", "HYDRAZINE", "METHANOL", "METHYLETHYLKETONE", "DICHLOROMETHANE", "METHYLFORMAMIDE", "METHYPYRROLIDINONE", "NITROBENZENE", "NITROGEN", "NITROMETHANE", "PHOSPHORYLCHLORIDE", "ISOPROPANOL", "PYRIDINE", "SULFOLANE", "TETRAHYDROFURAN", "TOLUENE", "TRIETHYLAMINE", "TRIFLUOROACETICACID", "WATER"};
double SOLVENT_EPS[48] = {6.19, 20.7, 37.5, 16.9, 6.8, 2.3, 13.1, 4.3, 17.5, 17.9, 12.4, 2.6, 2.2, 4.8, 2, 15, 9.8, 4.34, 2.2, 37, 46.7, 24.55, 6.02, 10.66, 37.7, 109.5, 58.5, 42.5, 43.3, 1.88, 51.7, 32.6, 18.5, 8.9, 182.4, 33, 34.8, 1.45, 35.87, 13.9, 19.9, 12.4, 43.3, 7.58, 2.38, 2.44, 8.55, 78.39};
double SOLVENT_RAD[48] = {2.83, 3.08, 2.76, 2.24, 3.31, 3.28, 3.45, 3.26, 3.31, 3.33, 3.35, 2.88, 3.37, 3.17, 3.5, 3.46, 3.54, 3.46, 3.24, 3.13, 3.04, 2.85, 3.39, 3.15, 2.81, 2.51, 2.47, 3.07, 4.1, 3.74, 2.33, 2.53, 3.3, 2.94, 2.86, 3.36, 3.44, 2.36, 2.77, 3.33, 3.12, 3.18, 3.35, 3.18, 3.48, 3.81, 3.12, 1.93};

/**
 * Helper conversion and trim functions
 * */
int startsWith(const char *str, const char *substr)
{
    if (str == NULL || substr == NULL) {
        return 0;
    }
    char *pos = strstr(str, substr);
    return pos == str;
}

char *toUpper(char *value)
{
    if (value == NULL) {
        return NULL;
    }
    char *upper = (char *)malloc(strlen(value) + 1); // +1 for null terminator
    if (upper == NULL) {
        return NULL; 
    }
    for (int inx = 0; inx < strlen(value); inx++)
    {
        upper[inx] = toupper(value[inx]);
    }
    upper[strlen(value)] = '\0';
    return upper;
}

int toInt(char *value)
{
    if (value == NULL) {
        return 0;
    }

    char *endptr;
    long result = strtoimax(value, &endptr, 10);

    if (value == endptr) {      
        return 0;
    }

    if (result > INT64_MAX || result < INT64_MIN) {
        return 0;
    }

    return (int)result;
}

double toDouble(char *value)
{
    if (value == NULL) {
        return 0.0;
    }
    char *endptr;
    double result = strtod(value, &endptr);

    if (value == endptr) {
        return 0.0;
    }

    return result;
}

char *convertUnits(char *value, double constant)
{
    if(value == NULL)
        return NULL;
    double source = toDouble(value) * constant;
    char *dest = (char *)malloc(100);
    if (dest == NULL) {
        return NULL;
    }
    snprintf(dest, 100, "%f", source);
    return dest;
}

size_t int2size_t(int val)
{
    return (val < 0) ? __SIZE_MAX__ : (size_t)((unsigned)val);
}

char *ltrim(char *value)
{
    if (value == NULL)
        return (NULL);

    int inx;
    for (inx = 0; inx < strlen(value); inx++)
    {
        if (value[inx] != ' ')
            return strndup(value + inx, int2size_t(strlen(value) - inx));
    }
    if (inx == strlen(value))
    {
        return "";
    }

    return value;
}

char *rtrim(char *value)
{
    if (value == NULL)
        return (NULL);

    int inx;
    for (inx = strlen(value) - 1; inx >= 0; inx--)
    {
        if (value[inx] != ' ')
            return strndup(value, int2size_t(inx + 1));
    }

    if (inx == -1)
        return "";

    return value;
}

char *trim(char *value)
{
    return (ltrim(rtrim(value)));
}

/**
 * Convert an array of doubles stored inside a string to another unit with a conversion factor.
 *
 * */

char *convertUnitsFromArray(char *array, int size, double conversion)
{
    if (array == NULL || size <= 0)
        return NULL;

    char *token;
    char *endptr;
    char *result = (char *)malloc(size * 21);
    if (result == NULL)
        return NULL; // Memory allocation failed

    char *saveptr;
    token = strtok_r(array, " ", &saveptr);
    if (token != NULL)
    {
        snprintf(result, size * 21, "%f ", (strtod(token, &endptr) * conversion));
        while ((token = strtok_r(NULL, " ", &saveptr)))
        {
            char temp[21];
            snprintf(temp, sizeof(temp), "%f ", (strtod(token, &endptr) * conversion));
            strncat(result, temp, size * 21 - strlen(result) - 1);
        }
    }
    return trim(result);
}

int stoichiometry[120];

void clearStoichiometry()
{
    for (int inx = 0; inx < 120; inx++)
        stoichiometry[inx] = 0;
}

char *getElementType(int atomicNumber)
{
    if (atomicNumber < 0 || atomicNumber >= 120) {
        return NULL;
    }
    char *type = malloc(3);
    if (type == NULL) {
        return NULL; // Memory allocation failed
    }
    strncpy(type, ELEMENT_TYPES[atomicNumber], 3);
    type[2] = '\0'; // Ensure null-termination
    return type;
}

char *getIndexAtomType(int index)
{
    if (index < 0 || index >= 120) {
        return NULL;
    }
    char *type = malloc(3);
    if (type == NULL) {
        return NULL; // Memory allocation failed
    }
    strncpy(type, ELEMENT_ALPHA[index], 3);
    type[2] = '\0'; // Ensure null-termination
    return type;
}

int getAtomTypeIndex(char *value)
{
    if (strcmp(value, "Ac") == 0)
    {
        return 1;
    }
    else if (strcmp(value, "Ag") == 0)
    {
        return 2;
    }
    else if (strcmp(value, "Al") == 0)
    {
        return 3;
    }
    else if (strcmp(value, "Am") == 0)
    {
        return 4;
    }
    else if (strcmp(value, "Ar") == 0)
    {
        return 5;
    }
    else if (strcmp(value, "As") == 0)
    {
        return 6;
    }
    else if (strcmp(value, "At") == 0)
    {
        return 7;
    }
    else if (strcmp(value, "Au") == 0)
    {
        return 8;
    }
    else if (strcmp(value, "B") == 0)
    {
        return 9;
    }
    else if (strcmp(value, "Ba") == 0)
    {
        return 10;
    }
    else if (strcmp(value, "Be") == 0)
    {
        return 11;
    }
    else if (strcmp(value, "Bh") == 0)
    {
        return 12;
    }
    else if (strcmp(value, "Bi") == 0)
    {
        return 13;
    }
    else if (strcmp(value, "Bk") == 0)
    {
        return 14;
    }
    else if (strcmp(value, "Br") == 0)
    {
        return 15;
    }
    else if (strcmp(value, "C") == 0)
    {
        return 16;
    }
    else if (strcmp(value, "Ca") == 0)
    {
        return 17;
    }
    else if (strcmp(value, "Cd") == 0)
    {
        return 18;
    }
    else if (strcmp(value, "Ce") == 0)
    {
        return 19;
    }
    else if (strcmp(value, "Cf") == 0)
    {
        return 20;
    }
    else if (strcmp(value, "Cl") == 0)
    {
        return 21;
    }
    else if (strcmp(value, "Cm") == 0)
    {
        return 22;
    }
    else if (strcmp(value, "Cn") == 0)
    {
        return 23;
    }
    else if (strcmp(value, "Co") == 0)
    {
        return 24;
    }
    else if (strcmp(value, "Cr") == 0)
    {
        return 25;
    }
    else if (strcmp(value, "Cs") == 0)
    {
        return 26;
    }
    else if (strcmp(value, "Cu") == 0)
    {
        return 27;
    }
    else if (strcmp(value, "Db") == 0)
    {
        return 28;
    }
    else if (strcmp(value, "Ds") == 0)
    {
        return 29;
    }
    else if (strcmp(value, "Dy") == 0)
    {
        return 30;
    }
    else if (strcmp(value, "Er") == 0)
    {
        return 31;
    }
    else if (strcmp(value, "Es") == 0)
    {
        return 32;
    }
    else if (strcmp(value, "Eu") == 0)
    {
        return 33;
    }
    else if (strcmp(value, "F") == 0)
    {
        return 34;
    }
    else if (strcmp(value, "Fe") == 0)
    {
        return 35;
    }
    else if (strcmp(value, "Fl") == 0)
    {
        return 36;
    }
    else if (strcmp(value, "Fm") == 0)
    {
        return 37;
    }
    else if (strcmp(value, "Fr") == 0)
    {
        return 38;
    }
    else if (strcmp(value, "Ga") == 0)
    {
        return 39;
    }
    else if (strcmp(value, "Gd") == 0)
    {
        return 40;
    }
    else if (strcmp(value, "Ge") == 0)
    {
        return 41;
    }
    else if (strcmp(value, "H") == 0)
    {
        return 42;
    }
    else if (strcmp(value, "He") == 0)
    {
        return 43;
    }
    else if (strcmp(value, "Hf") == 0)
    {
        return 44;
    }
    else if (strcmp(value, "Hg") == 0)
    {
        return 45;
    }
    else if (strcmp(value, "Ho") == 0)
    {
        return 46;
    }
    else if (strcmp(value, "Hs") == 0)
    {
        return 47;
    }
    else if (strcmp(value, "I") == 0)
    {
        return 48;
    }
    else if (strcmp(value, "In") == 0)
    {
        return 49;
    }
    else if (strcmp(value, "Ir") == 0)
    {
        return 50;
    }
    else if (strcmp(value, "K") == 0)
    {
        return 51;
    }
    else if (strcmp(value, "Kr") == 0)
    {
        return 52;
    }
    else if (strcmp(value, "La") == 0)
    {
        return 53;
    }
    else if (strcmp(value, "Li") == 0)
    {
        return 54;
    }
    else if (strcmp(value, "Lr") == 0)
    {
        return 55;
    }
    else if (strcmp(value, "Lu") == 0)
    {
        return 56;
    }
    else if (strcmp(value, "Lv") == 0)
    {
        return 57;
    }
    else if (strcmp(value, "Mc") == 0)
    {
        return 58;
    }
    else if (strcmp(value, "Md") == 0)
    {
        return 59;
    }
    else if (strcmp(value, "Mg") == 0)
    {
        return 60;
    }
    else if (strcmp(value, "Mn") == 0)
    {
        return 61;
    }
    else if (strcmp(value, "Mo") == 0)
    {
        return 62;
    }
    else if (strcmp(value, "Mt") == 0)
    {
        return 63;
    }
    else if (strcmp(value, "N") == 0)
    {
        return 64;
    }
    else if (strcmp(value, "Na") == 0)
    {
        return 65;
    }
    else if (strcmp(value, "Nb") == 0)
    {
        return 66;
    }
    else if (strcmp(value, "Nd") == 0)
    {
        return 67;
    }
    else if (strcmp(value, "Ne") == 0)
    {
        return 68;
    }
    else if (strcmp(value, "Nh") == 0)
    {
        return 69;
    }
    else if (strcmp(value, "Ni") == 0)
    {
        return 70;
    }
    else if (strcmp(value, "No") == 0)
    {
        return 71;
    }
    else if (strcmp(value, "Np") == 0)
    {
        return 72;
    }
    else if (strcmp(value, "O") == 0)
    {
        return 73;
    }
    else if (strcmp(value, "Og") == 0)
    {
        return 74;
    }
    else if (strcmp(value, "Os") == 0)
    {
        return 75;
    }
    else if (strcmp(value, "P") == 0)
    {
        return 76;
    }
    else if (strcmp(value, "Pa") == 0)
    {
        return 77;
    }
    else if (strcmp(value, "Pb") == 0)
    {
        return 78;
    }
    else if (strcmp(value, "Pd") == 0)
    {
        return 79;
    }
    else if (strcmp(value, "Pm") == 0)
    {
        return 80;
    }
    else if (strcmp(value, "Po") == 0)
    {
        return 81;
    }
    else if (strcmp(value, "Pr") == 0)
    {
        return 82;
    }
    else if (strcmp(value, "Pt") == 0)
    {
        return 83;
    }
    else if (strcmp(value, "Pu") == 0)
    {
        return 84;
    }
    else if (strcmp(value, "Ra") == 0)
    {
        return 85;
    }
    else if (strcmp(value, "Rb") == 0)
    {
        return 86;
    }
    else if (strcmp(value, "Re") == 0)
    {
        return 87;
    }
    else if (strcmp(value, "Rf") == 0)
    {
        return 88;
    }
    else if (strcmp(value, "Rg") == 0)
    {
        return 89;
    }
    else if (strcmp(value, "Rh") == 0)
    {
        return 90;
    }
    else if (strcmp(value, "Rn") == 0)
    {
        return 91;
    }
    else if (strcmp(value, "Ru") == 0)
    {
        return 92;
    }
    else if (strcmp(value, "S") == 0)
    {
        return 93;
    }
    else if (strcmp(value, "Sb") == 0)
    {
        return 94;
    }
    else if (strcmp(value, "Sc") == 0)
    {
        return 95;
    }
    else if (strcmp(value, "Se") == 0)
    {
        return 96;
    }
    else if (strcmp(value, "Sg") == 0)
    {
        return 97;
    }
    else if (strcmp(value, "Si") == 0)
    {
        return 98;
    }
    else if (strcmp(value, "Sm") == 0)
    {
        return 99;
    }
    else if (strcmp(value, "Sn") == 0)
    {
        return 100;
    }
    else if (strcmp(value, "Sr") == 0)
    {
        return 101;
    }
    else if (strcmp(value, "Ta") == 0)
    {
        return 102;
    }
    else if (strcmp(value, "Tb") == 0)
    {
        return 103;
    }
    else if (strcmp(value, "Tc") == 0)
    {
        return 104;
    }
    else if (strcmp(value, "Te") == 0)
    {
        return 105;
    }
    else if (strcmp(value, "Th") == 0)
    {
        return 106;
    }
    else if (strcmp(value, "Ti") == 0)
    {
        return 107;
    }
    else if (strcmp(value, "Tl") == 0)
    {
        return 108;
    }
    else if (strcmp(value, "Tm") == 0)
    {
        return 109;
    }
    else if (strcmp(value, "Ts") == 0)
    {
        return 110;
    }
    else if (strcmp(value, "U") == 0)
    {
        return 111;
    }
    else if (strcmp(value, "V") == 0)
    {
        return 112;
    }
    else if (strcmp(value, "W") == 0)
    {
        return 113;
    }
    else if (strcmp(value, "Xe") == 0)
    {
        return 114;
    }
    else if (strcmp(value, "Y") == 0)
    {
        return 115;
    }
    else if (strcmp(value, "Yb") == 0)
    {
        return 116;
    }
    else if (strcmp(value, "Zn") == 0)
    {
        return 117;
    }
    else if (strcmp(value, "Zr") == 0)
    {
        return 118;
    }
}

void addAtomType(char *elementType)
{
    stoichiometry[getAtomTypeIndex(elementType)] = stoichiometry[getAtomTypeIndex(elementType)] + 1;
}

char *addAtomToFormula(char *formula, char *type, int count)
{
    if (count == 1)
    {
        sprintf(formula, "%s%s", formula, type);
    }
    else
    {
        sprintf(formula, "%s%s%d", formula, type, count);
    }
    return formula;
}

char *getStoichiometry()
{
    char *formula;
    formula = (char *)malloc(50000);
    if (stoichiometry[16] != 0) // C atom type
        formula = addAtomToFormula(formula, "C", stoichiometry[16]);
    if (stoichiometry[42] != 0) // H atom type
        formula = addAtomToFormula(formula, "H", stoichiometry[42]);
    for (int inx = 1; inx < 120; inx++)
        if (inx != 16 && inx != 42 && stoichiometry[inx] != 0)
            formula = addAtomToFormula(formula, getIndexAtomType(inx), stoichiometry[inx]);
    return formula;
}

char *getSolventName(char *uppercaseName)
{
    for (int inx = 0; inx < 48; inx++)
    {
        if (strcmp(uppercaseName, SOLVENT_NAME_UPPERCASE[inx]) == 0)
            return SOLVENT_NAME[inx];
    }
    return NULL;
}

double getSolventRad(char *solvent)
{
    for (int inx = 0; inx < 48; inx++)
    {
        if (strcmp(solvent, SOLVENT_NAME[inx]) == 0)
            return SOLVENT_RAD[inx];
    }
   return 0;
}

double getSolventEps(char *solvent)
{
  for (int inx = 0; inx < 48; inx++)
    {
        if (strcmp(solvent, SOLVENT_NAME[inx]) == 0)
            return SOLVENT_EPS[inx];
    }
   return 0;
}