/***************************************************************************
 JumboRkf.c - Utility library that provides customized functions for reading
              ADF binary file format files via the KFc utility tool.
 
 Copyright (C) 2022 by Institute of Chemical Reasearch of Catalonia (ICIQ)
 For more info, contact ioChem-BD software team (contact at iochem-bd . com)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation version 3 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 ICIQ owns the intellectual property right for this file and reserves the
 right to distrbute it under a license other than LGPL
 ****************************************************************************/


#include "KFc.h"
#include "JumboHelper.h"
#include "JumboRkf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char variableTypes[4][8] = {"integer", "double", "string", "logical"};

void openADFFile(KFFile *kf, char *filepath)
{
    if (openKFFile(kf, filepath) < 0)
    {
        printf("Error opening rkf file\n\n\n");
        exit(1);
    }
}

// Debug function
int printVariable(KFFile kf, char *field)
{
    int type = getKFVariableType(&kf, field);
    printf("\n\n\n%s\n", field);
    printf("=======================\n");
    printf("Data type: %i (%s)\n", type, variableTypes[type - 1]);
    printf("Data length: %d, used %d\n\n", getKFVariableLength(&kf, field), getKFVariableUsedLength(&kf, field));
    return type;
}

// Debug function
void printValue(int dataType, double value)
{
    if (dataType == KF_T_INTEGER)
        printf("%d\t", value);
    else if (dataType == KF_T_DOUBLE)
        printf("%f\t", value);
    else if (dataType == KF_T_STRING)
        printf("%s\t", value);
    else if (dataType == KF_T_LOGICAL)
        printf("%i\t", value);
}

char *printValuesWithDelimiter(KFFile kf, char *field, char delimiter, int fieldtype)
{
    int *idata;
    double *ddata;
    char *cdata;

    int datasize;

#ifdef DEBUG
    printVariable(kf, field);
#endif
    int datatype = getKFVariableType(&kf, field);

    if (datatype == 0)
        return NULL;

    datasize = getKFVariableLength(&kf, field);
    if (datasize == -1)
        return NULL;

    if (datatype == KF_T_INTEGER || datatype == KF_T_LOGICAL)
    {
        idata = malloc(datasize * sizeof(int));
        if (getKFData(&kf, field, (void *)idata) < 0)
        {
            printf("Could not read anything!!!\n\n\n");
            return NULL;
        }
    }
    else if (datatype == KF_T_DOUBLE)
    {
        ddata = malloc(datasize * sizeof(double));
        if (getKFData(&kf, field, (void *)ddata) < 0)
        {
            printf("Could not read anything!!!\n\n\n");
            return NULL;
        }
    }
    else if (datatype == KF_T_STRING)
    {
        cdata = malloc(datasize * sizeof(char));
        if (getKFData(&kf, field, (void *)cdata) < 0)
        {
            printf("Could not read anything!!!\n\n\n");
            return NULL;
        }
    }

    int inx = 0;
    int elements = getKFVariableUsedLength(&kf, field);
    char *result;

    result = (char *)malloc(sizeof(char) * 21 * elements); // Will reserve 20 positions + delimiter * #elements of chars to have enough space
    result[0] = '\0';
    char line[500];

    for (inx = 0; inx < elements; inx++)
    {
        if (datatype == KF_T_INTEGER)
        {
            sprintf(line, "%d", idata[inx]);
            strcat(result, line);
        }
        else if (datatype == KF_T_DOUBLE)
        {
            sprintf(line, "%0.8f", ddata[inx]);
            strcat(result, line);
        }
        else if (datatype == KF_T_LOGICAL)
        {
            sprintf(line, "%i", idata[inx]);
            strcat(result, line);
        }
        else if (datatype == KF_T_STRING)
        {
            if (cdata[inx] == '\n' && fieldtype != KF_RT_SCALAR)
            {
                sprintf(line, "%c", delimiter);
            }
            else
            {
                sprintf(line, "%c", cdata[inx]);
            }
            strcat(result, line);
        }

        if (datatype != KF_T_STRING && inx != elements - 1 && fieldtype != KF_RT_SCALAR)
        {
            sprintf(line, "%c", delimiter);
            strcat(result, line);
        }
    }
    return trim(result);
}

char *printScalar(KFFile kf, char *field)
{
    return printValuesWithDelimiter(kf, field, '\0', KF_RT_SCALAR);
}

char *printArray(KFFile kf, char *field)
{
    return printValuesWithDelimiter(kf, field, ' ', KF_RT_ARRAY);
}

char *printArrayWithDelimiter(KFFile kf, char *field, char delimiter)
{
    return printValuesWithDelimiter(kf, field, delimiter, KF_RT_ARRAY);
}

char *normalizeStringArray(char *array)
{
    char *token;
    char *result;

    result = (char*) malloc(strlen(array));

    token = strtok_r(array, " ", &array);
    if (token != NULL)
    {
        sprintf(result, "%s ", token);        
        while ((token = strtok_r(array, " ", &array)))
            sprintf(result, "%s%s ", result, token);
    }
    return trim(result);
}
