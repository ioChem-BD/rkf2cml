 /***************************************************************************
 JumboRkf.h - header file for JumboRkf: Utility library that provides customized
              functions for reading ADF binary file format files via the
              KFc utility tool

 Copyright (C) 2022 by Institute of Chemical Reasearch of Catalonia (ICIQ)
 For more info, contact ioChem-BD software team (contact at iochem-bd . com)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation version 3 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 ICIQ owns the intellectual property right for this file and reserves the
 right to distrbute it under a license other than LGPL
 ****************************************************************************/

#ifndef _JRKF_H_
#define _JRKF_H_

/* readed field types */
#define KF_RT_SCALAR      1
#define KF_RT_ARRAY       2
#define KF_RT_MATRIX      3

#include "KFc.h"

void openADFFile(KFFile* kf, char *path);

char* printScalar(KFFile kf, char *field);

char* printArray(KFFile kf, char *field);

char* printArrayWithDelimiter(KFFile kf, char *field, char delimiter);

char* printValuesWithDelimiter(KFFile kf,char *field, char delimiter, int fieldtype);

char* normalizeStringArray(char *array);

#endif