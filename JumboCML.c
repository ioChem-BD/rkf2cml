/***************************************************************************
 JumboCML.c - CML file creation utility from ADF files in .rkf format.
              It uses MiniXML library to handle XML generation and saving.

 Copyright (C) 2022 by Institute of Chemical Reasearch of Catalonia (ICIQ)
 For more info, contact ioChem-BD software team (contact at iochem-bd . com)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation version 3 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 ICIQ owns the intellectual property right for this file and reserves the
 right to distrbute it under a license other than LGPL
 ****************************************************************************/

#include <inttypes.h>
#include <math.h>
#include "JumboCML.h"
#include "JumboHelper.h"
#include "JumboRkf.h"
#include "KFc.h"
#include <mxml.h>


int indentation = 0;

int numAtoms;
char *atomsString;
char atomsArray[JCML_MAX_ATOMS][3];

char *indent(int level)
{
  if (level <= 0)
    return "\n";

  if (level >= JCML_MAX_INDENTATION_LEVEL)
  {
    printf("Max indentation level has been reached");
    exit(1);
  }

  char *str = malloc(JCML_MAX_INDENTATION_LEVEL);
  memset(str, '\0', JCML_MAX_INDENTATION_LEVEL);
  memset(str, '\t', level);
  return str;
}

const char *whitespace_cb(void *cbdata, mxml_node_t *node, mxml_ws_t where)
{
  if (node == NULL) {
    return NULL; 
  }
  const char *element;
  element = mxmlGetElement(node);

  if (element == NULL) {
    return NULL;  
  }

  if (strcmp(element, "module") == 0 ||
      strcmp(element, "parameterList") == 0 ||
      strcmp(element, "propertyList") == 0 ||
      strcmp(element, "list") == 0 ||
      strcmp(element, "parameter") == 0 ||
      strcmp(element, "property") == 0 ||
      strcmp(element, "molecule") == 0 ||
      strcmp(element, "atomArray") == 0 ||
      strcmp(element, "bondArray") == 0)
  {
    switch (where)
    {
    case MXML_WS_BEFORE_OPEN:
      return indent(indentation++);
    case MXML_WS_AFTER_OPEN:
      return "\n";
    case MXML_WS_BEFORE_CLOSE:
      return indent(--indentation);
    case MXML_WS_AFTER_CLOSE:
      return indentation == 0 ? "" : "\n";
    default:
      return (NULL);
    }
  }
  else if (strcmp(element, "scalar") == 0 ||
           strcmp(element, "array") == 0 ||
           strcmp(element, "matrix") == 0)
  {
    switch (where)
    {
    case MXML_WS_BEFORE_OPEN:
      return indent(indentation);
    case MXML_WS_AFTER_OPEN:
      return (NULL);
    case MXML_WS_BEFORE_CLOSE:
      return (NULL);
    case MXML_WS_AFTER_CLOSE:
      return "\n";
    default:
      return (NULL);
    }
  }
  else if (strcmp(element, "formula") == 0)
  {
    switch (where)
    {
    case MXML_WS_BEFORE_OPEN:
      return indent(indentation);
    case MXML_WS_AFTER_OPEN:
      return "\n";
    case MXML_WS_BEFORE_CLOSE:
      return (NULL);
    case MXML_WS_AFTER_CLOSE:
      return "\n";
    default:
      return (NULL);
    }
  }
  else if (strcmp(element, "atom") == 0 ||
           strcmp(element, "bond") == 0)
  {
    switch (where)
    {
    case MXML_WS_BEFORE_OPEN:
      return indent(indentation);
    case MXML_WS_AFTER_OPEN:
      return "\n";
    case MXML_WS_BEFORE_CLOSE:
      return (NULL);
    case MXML_WS_AFTER_CLOSE:
      return "\n"; 
    default:
      return (NULL);
    }
  }
  return (NULL);
}

void getAtomTypesArray(KFFile amsrkf)
{
  numAtoms = toInt(printScalar(amsrkf, "Molecule%nAtoms"));

  if (numAtoms >= JCML_MAX_ATOMS)
  {
    printf("Reached max number of read atoms.");
    exit(1);
  }

  char *atomicNumbers = printArray(amsrkf, "Molecule%AtomicNumbers");
  char *line = malloc(numAtoms * 4);
  line[0]='\0';
  atomsString = malloc(numAtoms * 4);
  atomsString[0] = '\0';
  char *token;
  int inx = 1;
  // Load each atom type inside an array and as a string of atom types
  while ((token = strtok_r(atomicNumbers, " ", &atomicNumbers)))
  {
    char *type = getElementType(toInt(token));
    sprintf(line, "%s %s", line, type);
    atomsArray[inx][0] = type[0];
    atomsArray[inx][1] = type[1];
    atomsArray[inx][2] = type[2];
    inx++;
  }
  atomsString = trim(line);
}

void initializeCommonParameters(KFFile amsrkf)
{
  getAtomTypesArray(amsrkf);
}

mxml_node_t *createRootModule(mxml_node_t *xml)
{
  mxml_node_t *root;

  root = mxmlNewElement(xml, "module");
  mxmlElementSetAttr(root, "xmlns", "http://www.xml-cml.org/schema");
  mxmlElementSetAttr(root, "xmlns:a", "http://www.iochem-bd.org/dictionary/adf/");
  mxmlElementSetAttr(root, "xmlns:cc", "http://www.xml-cml.org/dictionary/compchem/");
  mxmlElementSetAttr(root, "xmlns:cml", "http://www.xml-cml.org/schema");
  mxmlElementSetAttr(root, "xmlns:cmlx", "http://www.xml-cml.org/schema/cmlx");
  mxmlElementSetAttr(root, "xmlns:convention", "http://www.xml-cml.org/convention/");
  mxmlElementSetAttr(root, "xmlns:nonsi", "http://www.xml-cml.org/unit/nonSi/");
  mxmlElementSetAttr(root, "xmlns:nonsi2", "http://www.iochem-bd.org/unit/nonSi2/");
  mxmlElementSetAttr(root, "xmlns:si", "http://www.xml-cml.org/unit/si/");
  mxmlElementSetAttr(root, "xmlns:xi", "http://www.w3.org/2001/XInclude");
  mxmlElementSetAttr(root, "xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
  mxmlElementSetAttr(root, "convention", "convention:compchem");
  mxmlElementSetAttr(root, "id", "rkf");
  return root;
}

mxml_node_t *createElement(mxml_node_t *parent, char *name, char *templateRef, char *dictRef, char *id, int numAttributes, char **attributeNames, char **attributeValues)
{
  mxml_node_t *element;
  int inx;

  element = mxmlNewElement(parent, name);
  if (templateRef != NULL)
    mxmlElementSetAttr(element, "cmlx:templateRef", templateRef);
  if (dictRef != NULL)
    mxmlElementSetAttr(element, "dictRef", dictRef);
  if (id != NULL)
    mxmlElementSetAttr(element, "id", id);

  for (inx = 0; inx < numAttributes; inx++)
    mxmlElementSetAttr(element, attributeNames[inx], attributeValues[inx]);
  return element;
}

mxml_node_t *createModule(mxml_node_t *parent, char *templateRef, char *dictRef, char *id)
{
  return createElement(parent, "module", templateRef, dictRef, id, 0, NULL, NULL);
}

/**
 *
 * Helper functions
 */

void createScalar(mxml_node_t *parent, char *dataType, char *units, char *dictRef, char *id, char *value)
{
  mxml_node_t *scalar;
  scalar = mxmlNewElement(parent, "scalar");
  if (dataType != NULL)
    mxmlElementSetAttr(scalar, "dataType", dataType);
  if (units != NULL)
    mxmlElementSetAttr(scalar, "units", units);
  if (dictRef != NULL)
    mxmlElementSetAttr(scalar, "dictRef", dictRef);
  if (id != NULL)
    mxmlElementSetAttr(scalar, "id", id);

  mxmlNewText(scalar, 0, value);
}

void createArray(mxml_node_t *parent, char *dataType, char *units, char *dictRef, int size, char *value)
{
  mxml_node_t *array;
  char line[10];
  if (size == -1)
    return;
  array = mxmlNewElement(parent, "array");
  if (dataType != NULL)
    mxmlElementSetAttr(array, "dataType", dataType);
  if (units != NULL)
    mxmlElementSetAttr(array, "units", units);
  if (dictRef != NULL)
    mxmlElementSetAttr(array, "dictRef", dictRef);
  sprintf(line, "%d", size);
  mxmlElementSetAttr(array, "size", line);
  mxmlNewText(array, 0, value);
}

void appendParameter(mxml_node_t *parent, char *dictRef, char *dataType, char *id, char *text)
{
  if (text == NULL || strcmp(text, "") == 0)
    return;

  mxml_node_t *parameter, *scalar;
  parameter = mxmlNewElement(parent, "parameter");
  if (dictRef != NULL)
    mxmlElementSetAttr(parameter, "dictRef", dictRef);

  createScalar(parameter, dataType, NULL, NULL, id, text);
}

void appendProperty(mxml_node_t *parent, char *dictRef, char *units, char *dataType, char *text)
{
  if (text == NULL || strcmp(text, "") == 0)
    return;

  mxml_node_t *property, *scalar;
  property = mxmlNewElement(parent, "property");
  if (dictRef != NULL)
    mxmlElementSetAttr(property, "dictRef", dictRef);

  if (dataType != NULL)
    mxmlElementSetAttr(property, "dataType", dataType);

  createScalar(property, NULL, units, NULL, NULL, text);
}

void appendMolecule(KFFile amsrkf, mxml_node_t *parent, char *sectionLabel, char *id)
{
  mxml_node_t *molecule;
  mxml_node_t *atomArray;
  mxml_node_t *bondArray;
  mxml_node_t *atom;
  mxml_node_t *bond;
  mxml_node_t *formula;

  char line[40];
  char *endptr;
  char *token;
  char *to;
  char *from;
  char *elementType;
  double x, y, z;
  double molMass;

  molecule = createElement(parent, "molecule", NULL, NULL, id, 0, NULL, NULL);
  atomArray = createElement(molecule, "atomArray", NULL, NULL, NULL, 0, NULL, NULL);

  // Retrieve all atom and bond information
  sprintf(line, "%s%s", sectionLabel, "%Coords");
  char *xyz = printArray(amsrkf, line);

  sprintf(line, "%s%s", sectionLabel, "%AtomicNumbers");
  char *typeInx = printArray(amsrkf, line);

  sprintf(line, "%s%s", sectionLabel, "%AtomMasses");
  char *atomMass = printArray(amsrkf, line);

  sprintf(line, "%s%s", sectionLabel, "%fromAtoms");
  char *fromBond = printArray(amsrkf, line);

  sprintf(line, "%s%s", sectionLabel, "%toAtoms");
  char *toBond = printArray(amsrkf, line);

  // Will now iterate atom type index
  int type;
  int index = 1;
  clearStoichiometry();

  while ((token = strtok_r(typeInx, " ", &typeInx)))
  {
    type = strtoimax(token, &endptr, 10);
    elementType = getElementType(type);

    molMass = molMass + (strtod(strtok_r(atomMass, " ", &atomMass), &endptr));

    x = strtod(strtok_r(xyz, " ", &xyz), &endptr) * JCML_BOHR_TO_ANGSTROM;
    y = strtod(strtok_r(xyz, " ", &xyz), &endptr) * JCML_BOHR_TO_ANGSTROM;
    z = strtod(strtok_r(xyz, " ", &xyz), &endptr) * JCML_BOHR_TO_ANGSTROM;

    atom = mxmlNewElement(atomArray, "atom");
    mxmlElementSetAttr(atom, "elementType", elementType);
    sprintf(line, "a%d", index++);
    mxmlElementSetAttr(atom, "id", line);
    sprintf(line, "%f", x);
    mxmlElementSetAttr(atom, "x3", line);
    sprintf(line, "%f", y);
    mxmlElementSetAttr(atom, "y3", line);
    sprintf(line, "%f", z);
    mxmlElementSetAttr(atom, "z3", line);
    addAtomType(elementType);
  }

  if(toBond != NULL && fromBond != NULL) {
    bondArray = createElement(molecule, "bondArray", NULL, NULL, NULL, 0, NULL, NULL);
    while ((to = strtok_r(toBond, " ", &toBond)))
    {
      from = strtok_r(fromBond, " ", &fromBond);
      bond = mxmlNewElement(bondArray, "bond");
      sprintf(line, "a%s a%s", to, from);
      mxmlElementSetAttr(bond, "atomRefs2", line);
      mxmlElementSetAttr(bond, "order", "S");
    }
  }

  char *formulaAttr = "concise";
  char *formulaStr = getStoichiometry();

  char **attr;
  char **attrVal;

  attr = &formulaAttr;
  attrVal = &formulaStr;

  formula = createElement(molecule, "formula", NULL, NULL, NULL, 1, attr, attrVal);

  sprintf(line, "%f", molMass);
  appendProperty(molecule, "cml:molmass", "nonsi:dalton", NULL, line);
}

/**
 *  Content generation functions
 */

void fillEnvironmentModule(KFFile adfrkf, KFFile amsrkf, mxml_node_t *parent)
{
  mxml_node_t *parameterList;
  parameterList = mxmlNewElement(parent, "parameterList");
  appendParameter(parameterList, "cc:program", "xsd:string", NULL, "AMS");

  // Read title from user input
  char *inputLines = printScalar(adfrkf, "General%user input");
  char *token;
  while (token = strtok_r(inputLines, "\n", &inputLines))
  {
    char *line = trim(toUpper(token));
    if (startsWith(line, "TITLE "))
    {
      char subtext[strlen(line)];
      for (int inx = 6; inx < strlen(line); inx++)
      {
        subtext[inx - 6] = token[inx];
      }
      subtext[strlen(line) - 6] = '\0';
      appendParameter(parameterList, "cc:title", "xsd:string", NULL, subtext);
    }
  }

  // the version field contains multiple values that have to be splitted
  char *version = strtok(printScalar(adfrkf, "General%release"), " ");
  appendParameter(parameterList, "cc:programVersion", "xsd:string", NULL, version);
  version = strtok(NULL, " ");
  appendParameter(parameterList, "cc:programSubversion", "xsd:string", NULL, version);
  version = strtok(NULL, "()");
  appendParameter(parameterList, "cc:compileDate", "xsd:string", NULL, version);
}

void appendInitialOtherComponents(KFFile adfrkf, KFFile amsrkf, mxml_node_t *parent)
{
  mxml_node_t *module;
  mxml_node_t *submodule;
  mxml_node_t *list;

  module = createModule(parent, NULL, "cc:userDefinedModule", "otherComponents");

  submodule = createModule(parent, "input.file", "cc:inputFile", NULL);
  char *lines = printScalar(amsrkf, "General%user input");
  char *token;
  while (token = strtok_r(lines, "\n", &lines))
  {
    createScalar(submodule, "xsd:string", NULL, "cc:inputLine", NULL, token);
  }

  submodule = createModule(module, "parameters2", NULL, NULL);
  list = createElement(submodule, "list", "scf", NULL, NULL, 0, NULL, NULL);
  char *lda = printScalar(adfrkf, "General%ldapot");
  if (strcmp(lda, "1") == 0)
    createScalar(list, "xsd:string", NULL, "cc:functional", NULL, "VWN");
  else if (strcmp(lda, "2") == 0)
    createScalar(list, "xsd:string", NULL, "cc:functional", NULL, "VWN+Stoll");

  list = createElement(submodule, "list", "spin", NULL, NULL, 0, NULL, NULL);
  char *spinMolecule = printScalar(adfrkf, "General%nspin");
  if (strcmp(spinMolecule, "1") == 0)
    createScalar(list, "xsd:string", NULL, "cc:spinMolecule", NULL, "Restricted");
  else if (strcmp(spinMolecule, "2") == 0)
    createScalar(list, "xsd:string", NULL, "cc:spinMolecule", NULL, "Unrestricted");

  spinMolecule = printScalar(adfrkf, "General%nspinf");
  if (strcmp(spinMolecule, "1") == 0)
    createScalar(list, "xsd:string", NULL, "cc:spinFragments", NULL, "Restricted");
  else if (strcmp(spinMolecule, "2") == 0)
    createScalar(list, "xsd:string", NULL, "cc:spinFragments", NULL, "Unrestricted");

  char *cosmoEner = printScalar(adfrkf, "COSMO%Bond Energy");
  if (cosmoEner != NULL)
  {
    mxml_node_t *list;
    submodule = createModule(module, "solvation", "cc:userDefinedModule", NULL);
    list = createElement(submodule, "list", NULL, NULL, "cosmo", 0, NULL, NULL);
    createScalar(list, "xsd:double", NULL, "a:cosmoArea", NULL, printScalar(adfrkf, "COSMO%Area"));
    createScalar(list, "xsd:double", NULL, "a:cosmoVolume", NULL, printScalar(adfrkf, "COSMO%Volume"));

    char *solvationParams = printScalar(adfrkf, "General%user input");
    char *solvent;
    char *token;
    char *value;
    int hasRad = 0;
    int hasEps = 0;
    int solvationSection = 0;

    while (token = strtok_r(solvationParams, "\n", &solvationParams))
    {
      token = trim(toUpper(token));

      if (startsWith(token, "SOLVATION"))
        solvationSection = 1;
      else if (solvationSection && startsWith(token, "END"))
        solvationSection = 0;

      if (solvationSection && startsWith(token, "EPS"))
      {
        value = strtok_r(token, "EPS", &token);
        createScalar(list, "xsd:double", NULL, "a:epsl", NULL, trim(value));
        hasEps = 1;
      }

      if (solvationSection && startsWith(token, "RAD"))
      {
        value = strtok_r(token, "RAD", &token);
        createScalar(list, "xsd:double", "nonsi:angstrom", "a:rsol", NULL, trim(value));
        hasRad = 1;
      }

      if (solvationSection && (startsWith(token, "SOLV NAME") || startsWith(token, "NAME")))
      {
        value = trim(strstr(token, "NAME") + 5);
        char *end = strstr(value, " ") == NULL ? value + strlen(value) : strstr(value, " ");
        solvent = (char *)malloc(100);
        for (int inx = 0; inx < end - value; inx++)
          solvent[inx] = value[inx];

        solvent = getSolventName(trim(solvent));
        createScalar(list, "xsd:double", "nonsi:angstrom", "a:solvent", NULL, solvent);
      }
    }

    if (!hasRad && solvent != NULL && getSolventRad(solvent))
    {
      char *value = malloc(100);
      sprintf(value, "%0.2f", getSolventRad(solvent));
      createScalar(list, "xsd:double", "nonsi:angstrom", "a:rsol", NULL, value);
    }

    if (!hasEps && solvent != NULL && getSolventEps(solvent))
    {
      char *value = malloc(100);
      sprintf(value, "%0.2f", getSolventEps(solvent));
      createScalar(list, "xsd:double", NULL, "a:epsl", NULL, value);
    }
  }
}

void fillInitializationModule(KFFile adfrkf, KFFile amsrkf, mxml_node_t *parent)
{
  mxml_node_t *parameterList;
  parameterList = mxmlNewElement(parent, "parameterList");
  char *calcType;

  if (printScalar(adfrkf, "GeoOpt%genrgy") != NULL)
    calcType = "GEOMETRY OPTIMIZATION\0";
  else
    calcType = "SINGLE POINT";

  appendParameter(parameterList, "cc:runtype", "xsd:string", NULL, calcType);

  if (printArray(adfrkf, "Vibrations%Frequencies[cm-1]") != NULL)
    appendParameter(parameterList, "cc:runtype", "xsd:string", NULL, "FREQUENCIES");

  appendParameter(parameterList, "cc:method", "xsd:string", "method", "DFT");
  appendParameter(parameterList, "cc:functional", "xsd:string", NULL, printScalar(adfrkf, "General%ggapot"));
  appendMolecule(adfrkf, parent, "Molecule", "geometry");
  appendInitialOtherComponents(adfrkf, amsrkf, parent);
}

void appendFrequencyAnalysisModule(KFFile adfrkf, mxml_node_t *parent)
{
  mxml_node_t *module, *freqs;
  if (printArray(adfrkf, "Vibrations%Frequencies[cm-1]") == NULL)
    return;

  freqs = createModule(parent, "adf.frequencyanalysis", NULL, NULL);

  char *masses = printArray(adfrkf, "Molecule%AtomMasses");
  module = createModule(freqs, "masses", NULL, NULL);
  createArray(module, "xsd:double", NULL, "cc:atomicmass", getKFVariableUsedLength(&adfrkf, "Molecule%AtomMasses"), printArray(adfrkf, "Molecule%AtomMasses"));
  createArray(module, "xsd:string", NULL, "cc:elementType", numAtoms, atomsString);


}

void appendSymmetryModule(KFFile adfrkf, mxml_node_t *parent)
{
  mxml_node_t *module;
  char value[10];

  module = createModule(createModule(parent, "adf.runtype", NULL, NULL), "symmetry", NULL, NULL);
  createScalar(module, "xsd:string", NULL, "a:symmetry", NULL, printScalar(adfrkf, "Symmetry%grouplabel"));
  sprintf(value, "%d", toInt(printScalar(adfrkf, "Molecule%Charge")));
  createScalar(module, "xsd:integer", NULL, "a:charge", NULL, value);

  char *mullikenSpin = printArray(adfrkf, "Properties%AtomSpinDen Mulliken");
  if (mullikenSpin != NULL)
  {
    double spinPolarization = 0.0;
    char *token;
    char *endptr;
    char spin[5];
    while (token = strtok_r(mullikenSpin, " ", &mullikenSpin))
      spinPolarization += toDouble(token);

    sprintf(spin, "%d", (int)round(spinPolarization));
    createScalar(module, "xsd:integer", NULL, "a:spinPolarization", NULL, spin);
  }
}

void appendConvergence(KFFile adfrkf, KFFile amsrkf, mxml_node_t *parent)
{
  mxml_node_t *module;

  if (printScalar(adfrkf, "GeoOpt%genrgy") != NULL)
  { // Geom. optimization
    char *result = printScalar(amsrkf, "General%termination status");
    if (strstr(result, "NORMAL TERMINATION") != NULL)
    { // It converged
      module = createModule(parent, "scf", NULL, NULL);
      createScalar(module, "xsd:string", NULL, "cc:scfConverged", NULL, "SCF CONVERGED");
    }
  }
}

void appendAdfModule(KFFile adfrkf, KFFile amsrkf, mxml_node_t *parent)
{
  mxml_node_t *module;
  module = createModule(parent, "adf", NULL, NULL);
  appendFrequencyAnalysisModule(adfrkf, module);
  appendSymmetryModule(adfrkf, module);
  appendConvergence(adfrkf, amsrkf, module);
}

void fillCalculationModule(KFFile adfrkf, KFFile amsrkf, mxml_node_t *parent)
{
  appendAdfModule(adfrkf, amsrkf, parent);
}

void appendFinalProperties(KFFile adfrkf, KFFile amsrkf, mxml_node_t *parent)
{
  appendProperty(parent, "cc:zeropoint", "nonsi:electronvolt", "xsd:double", convertUnits(printScalar(adfrkf, "Vibrations%ZeroPointEnergy"), JCML_AU_TO_EV));
  appendProperty(parent, "cc:cputime", NULL, "xsd:double", printScalar(amsrkf, "General%CPUTime"));
  appendProperty(parent, "cc:systemtime", NULL, "xsd:double", printScalar(amsrkf, "General%SysTime"));
  appendProperty(parent, "cc:elapsedtime", NULL, "xsd:double", printScalar(amsrkf, "General%ElapsedTime"));
}

void appendVibrations(KFFile adfrkf, KFFile amsrkf, mxml_node_t *parent)
{
  if (printArray(adfrkf, "Vibrations%Frequencies[cm-1]") == NULL)
    return;

  mxml_node_t *property;
  mxml_node_t *module;

  int numFreqs = getKFVariableUsedLength(&adfrkf, "Vibrations%Frequencies[cm-1]");
  char displacement[(17 * numAtoms * 3) + 1];
  char displacements[(17 * numFreqs * numAtoms * 3) + (numFreqs * 2)]; // Frequencies + delimiters
  char propertyName[100];

  displacements[0] = '\0';
  // Frequencies property
  property = mxmlNewElement(parent, "property");
  mxmlElementSetAttr(property, "dictRef", "cc:frequencies");

  module = createModule(property, "vibrations", "cc:vibrations", NULL);
  createArray(module, "xsd:double", "nonsi:cm-1", "cc:frequency", numFreqs, printArray(adfrkf, "Vibrations%Frequencies[cm-1]"));
  createArray(module, "xsd:string", NULL, "cc:elementType", numAtoms, atomsString);

  for (int inx = 1; inx <= numFreqs; inx++)
  {
    sprintf(propertyName, "Vibrations%%NoWeightNormalMode(%d)", inx);
    if (inx == numFreqs)
      sprintf(displacement, "%s", printArray(adfrkf, propertyName));
    else
      sprintf(displacement, "%s ", printArray(adfrkf, propertyName));

    strcat(displacements, displacement);
  }
  createArray(module, "xsd:double", NULL, "cc:displacement", (numFreqs * numAtoms * 3), displacements);

  // Intensities property
  property = mxmlNewElement(parent, "property");
  mxmlElementSetAttr(property, "dictRef", "cc:intensities");

  module = createModule(property, "intensities", NULL, NULL);
  createArray(module, "xsd:double", "nonsi2:km.mole-1", "cc:absortion", getKFVariableUsedLength(&adfrkf, "Vibrations%Intensities[km/mol]"), printArray(adfrkf, "Vibrations%Intensities[km/mol]"));
  createArray(module, "xsd:double", "nonsi2:angstrom4.amu-1", "a:raman", getKFVariableUsedLength(&adfrkf, "Vibrations%RamanIntens[A^4/amu]"), printArray(adfrkf, "Vibrations%RamanIntens[A^4/amu]"));
}

void appendThermochemistry(KFFile adfrkf, KFFile amsrkf, mxml_node_t *parent)
{
  mxml_node_t *module;
  mxml_node_t *submodule;
  mxml_node_t *property;
  mxml_node_t *child;

  if (printScalar(adfrkf, "Thermodynamics%Pressure") == NULL)
    return;

  property = mxmlNewElement(parent, "property");
  mxmlElementSetAttr(property, "dictRef", "cc:thermochemistry");

  module = createModule(property, "thermochemistry", NULL, NULL);

  createScalar(module, "xsd:double", "nonsi:atm", "cc:press", NULL, printScalar(adfrkf, "Thermodynamics%Pressure"));
  createScalar(module, "xsd:double", "si:k", "cc:temp", NULL, printScalar(adfrkf, "Thermodynamics%Temperature"));
  createArray(module, "xsd:double", NULL, "cc:moi", getKFVariableUsedLength(&adfrkf, "Thermodynamics%Moments of inertia"), printArray(adfrkf, "Thermodynamics%Moments of inertia"));
  createScalar(module, "xsd:integer", NULL, "cc:symmnumber", NULL, printScalar(adfrkf, "Symmetry%nsym"));
  createScalar(module, "xsd:string", NULL, "cc:pointgroup", NULL, printScalar(adfrkf, "Symmetry%grouplabel"));

  submodule = createModule(module, "energies", NULL, NULL);
  createScalar(submodule, "xsd:double", "si:k", "cc:temp", NULL, printScalar(adfrkf, "Thermodynamics%Temperature"));

  child = createElement(submodule, "list", "entropy", NULL, NULL, 0, NULL, NULL);
  createScalar(child, "xsd:double", "nonsi2:cal.mol-1.K-1", "cc:transl", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Entropy translational"), JCML_AU_TO_CAL_MOL_MINUS_ONE));
  createScalar(child, "xsd:double", "nonsi2:cal.mol-1.K-1", "cc:rotat", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Entropy rotational"), JCML_AU_TO_CAL_MOL_MINUS_ONE));
  createScalar(child, "xsd:double", "nonsi2:cal.mol-1.K-1", "cc:vibrat", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Entropy vibrational"), JCML_AU_TO_CAL_MOL_MINUS_ONE));
  createScalar(child, "xsd:double", "nonsi2:cal.mol-1.K-1", "cc:total", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Entropy total"), JCML_AU_TO_CAL_MOL_MINUS_ONE));

  child = createElement(submodule, "list", "internalEnergy", NULL, NULL, 0, NULL, NULL);
  createScalar(child, "xsd:double", "nonsi2:kcal.mol-1", "cc:transl", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Internal Energy translational"), JCML_AU_TO_KCAL_MOL_MINUS_ONE));
  createScalar(child, "xsd:double", "nonsi2:kcal.mol-1", "cc:rotat", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Internal Energy rotational"), JCML_AU_TO_KCAL_MOL_MINUS_ONE));
  createScalar(child, "xsd:double", "nonsi2:kcal.mol-1", "cc:vibrat", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Internal Energy vibrational"), JCML_AU_TO_KCAL_MOL_MINUS_ONE));
  createScalar(child, "xsd:double", "nonsi2:kcal.mol-1", "cc:total", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Internal Energy total"), JCML_AU_TO_KCAL_MOL_MINUS_ONE));

  child = createElement(submodule, "list", "heat", NULL, NULL, 0, NULL, NULL);
  createScalar(child, "xsd:double", "nonsi2:cal.mol-1.K-1", "cc:transl", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Heat Capacity translational"), JCML_AU_TO_CAL_MOL_MINUS_ONE));
  createScalar(child, "xsd:double", "nonsi2:cal.mol-1.K-1", "cc:rotat", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Heat Capacity rotational"), JCML_AU_TO_CAL_MOL_MINUS_ONE));
  createScalar(child, "xsd:double", "nonsi2:cal.mol-1.K-1", "cc:vibrat", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Heat Capacity vibrational"), JCML_AU_TO_CAL_MOL_MINUS_ONE));
  createScalar(child, "xsd:double", "nonsi2:cal.mol-1.K-1", "cc:total", NULL, convertUnits(printScalar(adfrkf, "Thermodynamics%Heat Capacity total"), JCML_AU_TO_CAL_MOL_MINUS_ONE));
}

void appendFinalOtherComponents(KFFile adfrkf, KFFile amsrkf, mxml_node_t *parent)
{
  mxml_node_t *module;
  mxml_node_t *submodule;
  mxml_node_t *list;
  module = createModule(parent, NULL, "cc:userDefinedModule", "otherComponents");

  // Atom type related variables
  char serial[10 * 2 + 100 * 3 + 1000 * 4 + 10000 * 5 + 1000000 * 6]; // Max. 1M atoms
  for (int inx = 1; inx <= numAtoms; inx++)
    sprintf(serial, "%s %d", serial, inx);
  // Modules section
  char *mdc = printArray(adfrkf, "Properties%MDC-m charges");
  if (mdc != NULL)
  {
    submodule = createModule(module, "atomic.charges", "cc:userDefinedModule", NULL);
    submodule = createElement(submodule, "list", "multipole", NULL, NULL, 0, NULL, NULL);

    createArray(submodule, "xsd:integer", NULL, "cc:serial", numAtoms, trim(serial));
    createArray(submodule, "xsd:string", NULL, "cc:elementType", numAtoms, atomsString);
    createArray(submodule, "xsd:double", NULL, "a:mdcm", numAtoms, mdc);
    createArray(submodule, "xsd:double", NULL, "a:mdcd", numAtoms, printArray(adfrkf, "Properties%MDC-d charges"));
    createArray(submodule, "xsd:double", NULL, "a:mdcq", numAtoms, printArray(adfrkf, "Properties%MDC-q charges"));
  }

  char *mdcma = printArray(adfrkf, "Properties%MDC-m charges_A");
  if (mdcma != NULL)
  {
    int elements = getKFVariableUsedLength(&adfrkf, "Properties%MDC-m charges_A");
    char *mdcmb = printArray(adfrkf, "Properties%MDC-m charges_B");
    char *mdcda = printArray(adfrkf, "Properties%MDC-d charges_A");
    char *mdcdb = printArray(adfrkf, "Properties%MDC-d charges_B");
    char *mdcqa = printArray(adfrkf, "Properties%MDC-q charges_A");
    char *mdcqb = printArray(adfrkf, "Properties%MDC-q charges_B");

    char spinm[elements * 20];
    char spind[elements * 20];
    char spinq[elements * 20];

    char densm[elements * 20];
    char densd[elements * 20];
    char densq[elements * 20];

    char *endptr;
    double valuem;
    double valued;
    double valueq;

    spinm[0] = '\0';
    spind[0] = '\0';
    spinq[0] = '\0';

    densm[0] = '\0';
    densd[0] = '\0';
    densq[0] = '\0';

    for (int inx = 0; inx < elements; inx++)
    {
      double ma = strtod(strtok_r(mdcma, " ", &mdcma), &endptr);
      double mb = strtod(strtok_r(mdcmb, " ", &mdcmb), &endptr);
      double da = strtod(strtok_r(mdcda, " ", &mdcda), &endptr);
      double db = strtod(strtok_r(mdcdb, " ", &mdcdb), &endptr);
      double qa = strtod(strtok_r(mdcqa, " ", &mdcqa), &endptr);
      double qb = strtod(strtok_r(mdcqb, " ", &mdcqb), &endptr);

      valuem = ma + mb;
      valued = da + db;
      valueq = qa + qb;
      sprintf(spinm, "%s %f", spinm, valuem);
      sprintf(spind, "%s %f", spind, valued);
      sprintf(spinq, "%s %f", spinq, valueq);

      valuem = mb - ma;
      valued = db - da;
      valueq = qb - qa;
      sprintf(densm, "%s %f", densm, valuem);
      sprintf(densd, "%s %f", densd, valued);
      sprintf(densq, "%s %f", densq, valueq);
    }

    submodule = createModule(module, "atomic.charges.spin", "cc:userDefinedModule", NULL);
    submodule = createElement(submodule, "list", "spin", NULL, NULL, 0, NULL, NULL);

    createArray(submodule, "xsd:integer", NULL, "cc:serial", numAtoms, trim(serial));
    createArray(submodule, "xsd:string", NULL, "cc:elementType", numAtoms, atomsString);
    createArray(submodule, "xsd:double", NULL, "a:mdcm", numAtoms, trim(spinm));
    createArray(submodule, "xsd:double", NULL, "a:mdcd", numAtoms, trim(spind));
    createArray(submodule, "xsd:double", NULL, "a:mdcq", numAtoms, trim(spinq));

    submodule = createModule(module, "spin.density", "cc:userDefinedModule", NULL);
    submodule = createElement(submodule, "list", "spinDensity", NULL, NULL, 0, NULL, NULL);

    createArray(submodule, "xsd:integer", NULL, "cc:serial", numAtoms, trim(serial));
    createArray(submodule, "xsd:string", NULL, "cc:elementType", numAtoms, atomsString);
    createArray(submodule, "xsd:double", NULL, "a:mdcm", numAtoms, trim(densm));
    createArray(submodule, "xsd:double", NULL, "a:mdcd", numAtoms, trim(densd));
    createArray(submodule, "xsd:double", NULL, "a:mdcq", numAtoms, trim(densq));
  }

  // Bonding energy section
  char *eener = printScalar(adfrkf, "Energy%Electrostatic Interaction");
  if (eener != NULL)
  {
    mxml_node_t *bonding;
    bonding = createModule(module, "bonding.energy", NULL, NULL);
    submodule = createModule(bonding, "summary", NULL, NULL);

    createScalar(submodule, "xsd:double", "nonsi:electronvolt", "cc:eener", NULL, convertUnits(eener,JCML_AU_TO_EV));
    createScalar(submodule, "xsd:double", "nonsi:electronvolt", "cc:kinener", NULL, convertUnits(printScalar(adfrkf, "Energy%Kinetic Energy"),JCML_AU_TO_EV));
    createScalar(submodule, "xsd:double", "nonsi:electronvolt", "cc:coulombener", NULL, convertUnits(printScalar(adfrkf, "Energy%Elstat Interaction"),JCML_AU_TO_EV));
    createScalar(submodule, "xsd:double", "nonsi:electronvolt", "cc:xcener", NULL, convertUnits(printScalar(adfrkf, "Energy%XC Energy"),JCML_AU_TO_EV));

    if (printScalar(adfrkf, "Energy%Solvation Energy (el)") && printScalar(adfrkf, "Energy%Solvation Energy (cd)"))
    {
      double solvener = toDouble(printScalar(adfrkf, "Energy%Solvation Energy (el)")) + toDouble(printScalar(adfrkf, "Energy%Solvation Energy (cd)"));
      char *solvstr = (char *)malloc(21);
      sprintf(solvstr, "%f", solvener);
      createScalar(submodule, "xsd:double", "nonsi:electronvolt", "cc:solvener", NULL, convertUnits(solvstr,JCML_AU_TO_EV));
    }

    createScalar(submodule, "xsd:double", "nonsi:electronvolt", "cc:dispener", NULL, convertUnits(printScalar(adfrkf, "Energy%Dispersion Energy"),JCML_AU_TO_EV));
    createScalar(submodule, "xsd:double", "nonsi:electronvolt", "cc:total", NULL, convertUnits(printScalar(adfrkf, "Energy%Bond Energy"),JCML_AU_TO_EV));
  }

  // Mulliken
  char *mulliken = printArray(adfrkf, "Properties%AtomCharge Mulliken");
  if (mulliken != NULL)
  {
    mxml_node_t *charges;
    mxml_node_t *list;

    submodule = createModule(module, "mulliken", NULL, NULL);
    charges = createModule(submodule, "charges", NULL, NULL);
    list = createElement(charges, "list", "row", NULL, "row", 0, NULL, NULL);
    createArray(list, "xsd:integer", NULL, "cc:serial", numAtoms, trim(serial));
    createArray(list, "xsd:string", NULL, "cc:elementType", numAtoms, atomsString);
    createArray(list, "xsd:double", NULL, "x:charge", getKFVariableUsedLength(&adfrkf, "Properties%AtomCharge Mulliken"), mulliken);
    createArray(list, "xsd:double", NULL, "a:spinDensity", getKFVariableUsedLength(&adfrkf, "Properties%AtomSpinDen Mulliken"), printArray(adfrkf, "Properties%AtomSpinDen Mulliken"));
  }

  //Dipole
  char *dipole = printArray(adfrkf, "Properties%Dipole");
  if (dipole != NULL)
  {
    char *token;
    double magnitude= 0.0;

    submodule = createModule(module, "dipole.moment", "cc:userDefinedModule", NULL);
    createArray(submodule, "xsd:double", NULL, "cc:dipole", getKFVariableUsedLength(&adfrkf, "Properties%Dipole"), dipole);
    while (token = strtok_r(dipole, " ", &dipole)){    
      magnitude+=pow(toDouble(dipole), 2);  
    }
    magnitude = sqrt(magnitude);

    char * magnitudestr = (char *)malloc(21);
    sprintf(magnitudestr, "%f", magnitude);
    createScalar(submodule, "xsd:double", NULL, "x:dipole", NULL, magnitudestr);
  }  

  // Quadrupole
  char *quadrupole = printArray(adfrkf, "Properties%Quadrupole");
  if (quadrupole != NULL)
  {
    submodule = createModule(module, "quadrupole.moment", "cc:userDefinedModule", NULL);
    createArray(submodule, "xsd:double", NULL, "cc:quadrupole", getKFVariableUsedLength(&adfrkf, "Properties%Quadrupole"), quadrupole);
  }

  // S**2
  char *ssquared = printScalar(adfrkf, "Properties%S2calc");
  if (ssquared != NULL)
  {
    submodule = createModule(module, "s2", "cc:userDefinedModule", NULL);
    createScalar(submodule, "xsd:double", NULL, "cc:s2", NULL, printScalar(adfrkf, "Properties%S2pure"));
    createScalar(submodule, "xsd:double", NULL, "cc:s2expected", NULL, ssquared);
  }

  // NMR - now disabled in favour of jumbo-saxon captured values
  char *shielding = printArray(adfrkf, "Properties%NMR Shieldings InputOrder");
  if (0 && shielding != NULL)
  {
    mxml_node_t *nmrmodule;
    char *token;
    nmrmodule = createModule(module, "nmr", "cc:userDefinedModule", NULL);
    int index = 1;
    int counter = 0;
    int elements = getKFVariableUsedLength(&adfrkf, "Properties%NMR Shieldings InputOrder");
    char *elementTypes = malloc(elements * 20);
    char *nucleus = malloc(elements * 20);
    char *total = malloc(elements * 20);

    while (token = strtok_r(shielding, " ", &shielding))
    {
      if (toDouble(token) != JCML_NMR_EMPTY_FIELD)
      {
        sprintf(elementTypes, "%s %s", elementTypes, atomsArray[index]);
        sprintf(nucleus, "%s %d", nucleus, index);
        sprintf(total, "%s %f", total, toDouble(token));
        counter++;
      }
      index++;
    }

    nmrmodule = createModule(nmrmodule, "nucleus", NULL, NULL);
    appendMolecule(amsrkf, nmrmodule, "Molecule", "coordinates.nmr");
    createArray(nmrmodule, "xsd:string", NULL, "cc:elementType", counter, trim(elementTypes));
    createArray(nmrmodule, "xsd:integer", NULL, "a:nucleus", counter, trim(nucleus));
    createArray(nmrmodule, "xsd:string", "nonsi2:ppm", "a:total", counter, trim(total));
  }

  // Scaled Orbitals ZORA
  char *symlab = printScalar(adfrkf, "Symmetry%symlab");
  if (symlab != NULL)
  {
    //Read Symmetry%symlabr
    char *symlabr = printScalar(adfrkf, "Symmetry%symlabr");

    mxml_node_t *orbitals;
    mxml_node_t *energyModule;
    char *label;
    char section[50];

    // Spin alpha or no spin
    char *serialA = (char *)malloc(50);
    char *occupationA = (char *)malloc(50);
    char *energyA = (char *)malloc(50);
    char *spinA = (char *)malloc(50);
    char *irrepA = (char *)malloc(50);
    int totalOrbA = 0;

    serialA[0] = '\0';
    occupationA[0] = '\0';
    energyA[0] = '\0';
    spinA[0] = '\0';
    irrepA[0] = '\0';

    // Spin beta
    char *serialB = (char *)malloc(50);
    char *occupationB = (char *)malloc(50);
    char *energyB = (char *)malloc(50);
    char *spinB = (char *)malloc(50);
    char *irrepB = (char *)malloc(50);
    int totalOrbB = 0;

    serialB[0] = '\0';
    occupationB[0] = '\0';
    energyB[0] = '\0';
    spinB[0] = '\0';
    irrepB[0] = '\0';

    int isOpenShell = 0;

    int hasAbr = symlabr!= NULL;
    while (label = ((symlabr == NULL)? strtok_r(symlab, " ", &symlab) : strtok_r(symlabr, " ", &symlabr)))
    {
      if(hasAbr){
        sprintf(section, "%s%s", label, "%nmo_A");
        if (printScalar(adfrkf, section) == NULL){          
          //Append a blank space before label content          
          size_t len = strlen(label);
          char* new_label = (char*) malloc(len + 2); 
          if (new_label == NULL) {              
            return;
          }

          new_label[0] = ' ';  // Add space at the beginning
          strcpy(new_label + 1, label);  // Copy original content after the space
          label=new_label;  // Free the original memory
        }
        if (symlabr == NULL || symlabr[0] == '\0') {
            hasAbr = 0;
            if (symlabr == NULL) {
                symlabr = (char *)malloc(sizeof(char));
            }
            symlabr[0] = '\0';
        }
      }


      // Alpha or closed shell
      sprintf(section, "%s%s", label, "%nmo_A");
      int numOrb = toInt(printScalar(adfrkf, section));
      totalOrbA += numOrb;

      sprintf(section, "%s%s", label, "%froc_A");
      occupationA = (char *)realloc(occupationA, sizeof(occupationA) + (totalOrbA * 21 * sizeof(char)));
      sprintf(occupationA, "%s %s", occupationA, printArray(adfrkf, section));

      sprintf(section, "%s%s", label, "%escale_A");
      energyA = (char *)realloc(energyA, sizeof(energyA) + (totalOrbA * 21 * sizeof(char)));
      sprintf(energyA, "%s %s", energyA, printArray(adfrkf, section));

      serialA = (char *)realloc(serialA, sizeof(serialA) + (totalOrbA * 10 * sizeof(char)));
      spinA = (char *)realloc(spinA, sizeof(spinA) + (totalOrbA * 10 * sizeof(char)));
      irrepA = (char *)realloc(irrepA, sizeof(irrepA) + (totalOrbA * 10 * sizeof(char)));

      for (int inx = 0; inx < numOrb; inx++)
      {
        // TODO: Serial can be upper numbers not serial ones starting from 1
        sprintf(serialA, "%s %d\0", serialA, inx + 1);
        sprintf(spinA, "%s %s", spinA, "A");
        sprintf(irrepA, "%s %s", irrepA, label);
      }

      // Beta
      sprintf(section, "%s%s", label, "%nmo_B");
      if (printScalar(adfrkf, section) != NULL)
      {
        isOpenShell = 1;
        int numOrb = toInt(printScalar(adfrkf, section));
        totalOrbB += numOrb;

        sprintf(section, "%s%s", label, "%froc_B");
        occupationB = (char *)realloc(occupationB, sizeof(occupationB) + (totalOrbB * 21 * sizeof(char)));
        sprintf(occupationB, "%s %s", occupationB, printArray(adfrkf, section));

        sprintf(section, "%s%s", label, "%eps_B");
        energyB = (char *)realloc(energyB, sizeof(energyB) + (totalOrbB * 21 * sizeof(char)));
        sprintf(energyB, "%s %s", energyB, printArray(adfrkf, section));

        serialB = (char *)realloc(serialB, sizeof(serialB) + (totalOrbB * 10 * sizeof(char)));
        spinB = (char *)realloc(spinB, sizeof(spinB) + (totalOrbB * 10 * sizeof(char)));
        irrepB = (char *)realloc(irrepB, sizeof(irrepB) + (totalOrbB * 10 * sizeof(char)));

        for (int inx = 0; inx < numOrb; inx++)
        {
          // TODO: Serial can be upper numbers not serial ones starting from 1
          sprintf(serialB, "%s %d", serialB, inx + 1);
          sprintf(spinB, "%s %s", spinB, "B");
          sprintf(irrepB, "%s %s", irrepB, label);
        }
      }
    }
    if (isOpenShell)
    {
      totalOrbA += totalOrbB;

      serialA = (char *)realloc(serialA, sizeof(serialA) + sizeof(serialB) + totalOrbA * 10 * sizeof(char));
      sprintf(serialA, "%s%s", serialA, serialB);

      occupationA = (char *)realloc(occupationA, sizeof(occupationA) + sizeof(occupationB) + totalOrbA * 21 * sizeof(char));
      sprintf(occupationA, "%s%s", occupationA, occupationB);

      energyA = (char *)realloc(energyA, sizeof(energyA) + sizeof(energyB) + totalOrbA * 21 * sizeof(char));
      sprintf(energyA, "%s%s", energyA, energyB);

      spinA = (char *)realloc(spinA, sizeof(spinA) + sizeof(spinB) + totalOrbA * 10 * sizeof(char));
      sprintf(spinA, "%s%s", spinA, spinB);

      irrepA = (char *)realloc(irrepA, sizeof(irrepA) + sizeof(irrepB) + totalOrbA * 10 * sizeof(char));
      sprintf(irrepA, "%s%s", irrepA, irrepB);

      orbitals = createModule(module, "orbital.energies.spin.zora", "cc:userDefinedModule", NULL);
    }
    else
    {
      orbitals = createModule(module, "orbital.energies.zora", "cc:userDefinedModule", NULL);
    }

    char *energyEv = convertUnitsFromArray(energyA, totalOrbA, JCML_AU_TO_EV);

    energyModule = createElement(orbitals, "list", "energies", "energies", NULL, 0, NULL, NULL);
    createArray(energyModule, "xsd:integer", NULL, "cc:serial", totalOrbA, trim(serialA));
    createArray(energyModule, "xsd:double", NULL, "cc:occup", totalOrbA, trim(occupationA));
    createArray(energyModule, "xsd:double", "nonsi:electronvolt", "cc:energy", totalOrbA, trim(energyEv));
    createArray(energyModule, "xsd:string", NULL, "cc:irrep", totalOrbA, trim(irrepA));
    if (isOpenShell)
      createArray(energyModule, "xsd:string", NULL, "cc:spin", totalOrbA, trim(spinA));
  }

  // Excitations
  char *symlabexc = printScalar(adfrkf, "Symmetry%symlab excitations");
  if (symlabexc != NULL)
  {
    mxml_node_t *excitation;
    mxml_node_t *submodule;

    char *label;
    char *serial;
    char section[50];

    excitation = createModule(module, "excitation.energy", "cc:userDefinedModule", NULL);
    while (label = strtok_r(symlabexc, " ", &symlabexc))
    {
      label = trim(label);
      sprintf(section, "Excitations SS %s%%excenergies", label);
      if (printArray(adfrkf, section) == NULL)
        continue;
      submodule = createModule(excitation, "excitationEnergies", NULL, NULL);
      createScalar(submodule, "xsd:string", NULL, "cc:symm", NULL, label);

      mxml_node_t *energies = createModule(submodule, "energies", NULL, NULL);
      sprintf(section, "Excitations SS %s%%nr of excenergies", label);
      int numberOfEnergies = toInt(printScalar(adfrkf, section));
      serial = (char *)malloc(54320);
      for (int inx = 1; inx <= numberOfEnergies; inx++)
        sprintf(serial, "%s %d", serial, inx);
      serial = trim(serial);
      createArray(energies, "xsd:integer", NULL, "cc:serial", numberOfEnergies, serial);

      sprintf(section, "Excitations SS %s%%excenergies", label);
      createArray(energies, "xsd:double", "nonsi:hartree", "cc:energy", numberOfEnergies, printArray(adfrkf, section));
      sprintf(section, "Excitations SS %s%%oscillator strengths", label);
      createArray(energies, "xsd:double", "nonsi:hartree", "cc:oscillator", numberOfEnergies, printArray(adfrkf, section));

      mxml_node_t *dipole = createModule(submodule, "dipole", NULL, NULL);
      char *mux = (char *)malloc(numberOfEnergies * 21 * sizeof(char));
      char *muy = (char *)malloc(numberOfEnergies * 21 * sizeof(char));
      char *muz = (char *)malloc(numberOfEnergies * 21 * sizeof(char));
      char *token;
      createArray(dipole, "xsd:integer", NULL, "cc:serial", numberOfEnergies, serial);
      sprintf(section, "Excitations SS %s%%transition dipole moments", label);
      char *moments = printArray(adfrkf, section);
      for (int inx = 0; inx < numberOfEnergies; inx++)
      {
        sprintf(mux, "%s %s", mux, strtok_r(moments, " ", &moments));
        sprintf(muy, "%s %s", muy, strtok_r(moments, " ", &moments));
        sprintf(muz, "%s %s", muz, strtok_r(moments, " ", &moments));
      }

      sprintf(section, "Excitations SS %s%%excenergies", label);
      createArray(dipole, "xsd:double", "nonsi:hartree", "cc:energy", numberOfEnergies, printArray(adfrkf, section));
      sprintf(section, "Excitations SS %s%%oscillator strengths", label);
      createArray(dipole, "xsd:double", "nonsi:hartree", "cc:oscillator", numberOfEnergies, printArray(adfrkf, section));
      createArray(dipole, "xsd:double", "nonsi:hartree", "cc:muX", numberOfEnergies, trim(mux));
      createArray(dipole, "xsd:double", "nonsi:hartree", "cc:muY", numberOfEnergies, trim(muy));
      createArray(dipole, "xsd:double", "nonsi:hartree", "cc:muZ", numberOfEnergies, trim(muz));
    }
  }

  // Log file
  char *potenergy = printScalar(adfrkf, "Energy%Bond Energy");
  if (potenergy != NULL)
  {
    char token[50];
    submodule = createModule(module, "logfile", "cc:userDefinedModule", NULL);
    createScalar(submodule, "xsd:double", "nonsi:hartree", "cc:potentialEnergy", NULL, potenergy);
    sprintf(token, "%f", strtod(potenergy, NULL) * JCML_AU_TO_EV);
    createScalar(submodule, "xsd:double", "nonsi:electronvolt", "cc:potentialEnergy", NULL, token);
    sprintf(token, "%d", toInt(printScalar(adfrkf, "Molecule%Charge")));
    createScalar(submodule, "xsd:integer", NULL, "a:charge", NULL, token);

    if (printScalar(adfrkf, "GeoOpt%genrgy") != NULL)
    {
      char *result = printScalar(amsrkf, "General%termination status");
      if (strstr(result, "NORMAL TERMINATION") != NULL)
        createScalar(submodule, "xsd:string", NULL, "a:converged", NULL, "GEOMETRY CONVERGED");
      else
        createScalar(submodule, "xsd:string", NULL, "a:converged", NULL, "GEOMETRY DID NOT CONVERGE");
    }
  }
}

void fillFinalizationModule(KFFile adfrkf, KFFile amsrkf, mxml_node_t *parent)
{
  mxml_node_t *module;
  appendMolecule(amsrkf, parent, "Molecule", "finalization");
  module = createElement(parent, "propertyList", NULL, NULL, NULL, 0, NULL, NULL);
  appendFinalProperties(adfrkf, amsrkf, module);

  appendVibrations(adfrkf, amsrkf, module);
  appendThermochemistry(adfrkf, amsrkf, module);
  appendFinalOtherComponents(adfrkf, amsrkf, parent);
}

void createCmlFile(char *adfpath, char *amspath, char *filepath)
{
  KFFile adfrkf;
  KFFile amsrkf;

  mxml_node_t *xml;
  mxml_node_t *root;
  mxml_node_t *jobList;
  mxml_node_t *job;
  mxml_node_t *module;

  openADFFile(&adfrkf, adfpath);
  openADFFile(&amsrkf, amspath);

  initializeCommonParameters(amsrkf);

  //Create the CML file
  mxml_options_t *options = mxmlOptionsNew();
  mxmlOptionsSetWrapMargin(options, 0);
  xml = mxmlNewXML("1.0");
  root = createRootModule(xml);
  jobList = createModule(root, NULL, "cc:jobList", "jobList1");

  job = createModule(jobList, "job", "cc:job", "job");
  module = createModule(job, NULL, "cc:environment", "environment");
  fillEnvironmentModule(adfrkf, amsrkf, module);

  module = createModule(job, NULL, "cc:initialization", "initialization");
  fillInitializationModule(adfrkf, amsrkf, module);

  module = createModule(job, NULL, "cc:calculation", "calculation");
  fillCalculationModule(adfrkf, amsrkf, module);

  module = createModule(job, NULL, "cc:finalization", "finalization");
  fillFinalizationModule(adfrkf, amsrkf, module);

  FILE *fp;

  fp = fopen(filepath, "w");


  mxmlOptionsSetWhitespaceCallback(options, whitespace_cb, NULL);
  mxmlSaveFile(xml, options, fp);
  fclose(fp);

  /* Close the rkf files */
  closeKFFile(&adfrkf);
  closeKFFile(&amsrkf);
}
