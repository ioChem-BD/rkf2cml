# rkf2cml

## ioChem-BD conversion tool from .rkf format to CML 

The current tool reads [SCM ADF software](https://www.scm.com) binary result files, previously named TAPE21, and converts them into a chemistry-oriented XML format, called [Chemical Markup Language](https://www.xml-cml.org/). These files will be later processed and inserted in the [ioChem-BD chemical repository](https://gitlab.com/ioChem-BD/iochem-bd) software.

The KF files are Direct-Access, binary, and keyword-driven files. To ease its management we take advantage of the [KFReader library](https://www.scm.com/support/downloads/) released by SCM under the LGPL license.

The aim for developing this tool was:
* To gradually migrate the ioChem-BD data capturing mechanism for SCM ADF software: from parsing its textual output content to using its .rkf binary files.
* To avoid updates from new SCM ADF releases breaking the textual data capturing templates.
* To speed up the conversion process by accessing raw data via direct indexes and not evaluating the entire textual document content.

The tool is compiled and linked statically with its dependencies to be a standalone/portable executable file.

### Build dependencies 

The tool requires a C compiler like *gcc* or *cc*, the *make* tool and the  following libraries:

* Static build of the latest **MiniXML**: To create and compose XML files. 
* Static **glibc** libraries : To bundle the static C libraries from the used dependencies.


Installation of MiniXML for RedHat, CentOS, Fedora platforms:

```shell
dnf install glibc-static 
git clone https://github.com/michaelrsweet/mxml
cd mxml
git checkout v4.0.3
 ./configure -prefix=/usr
make
sudo make install
```

Installation for Ubuntu platform:

```shell
apt-get install libc6-dev
git clone https://github.com/michaelrsweet/mxml
cd mxml
git checkout v4.0.3
 ./configure -prefix=/usr
make
sudo make install
```

**Note**: To install the mxml library on Mac OSes we will set *-prefix=/usr/local* for system security reasons. In this situation, current project *Makefile* will detect OS type and point that folder instead of the predefined one.

### Building the tool

Just clone the project and run *make* tool and the process will compile and generate the *rfk2cml* file. Copy then that file into the [ioChem-BD shell client](https://docs.iochem-bd.org/doc/guides/usage/uploading-content-to-create/using-shell-client.html) folder.

```shell
git clone https://gitlab.com/ioChem-BD/rkf2cml
cd rkf2cml
make all
```

## Running the tool

The tool requires three parameters, the *ams.rkf* and *adf.rkf* file paths and the path where the *output* CML file will be generated.

```shell 
Usage: ./rkf2cml rkffile1 rkffile2 outfile

        Mandatory parameters;
            rkffile1: Full path to the ams.rkf file
            rkffile2: Full path to the adf.rkf file
            outfile: Path to the generated output file

        Example:
           ./rkf2cml /opt/calc1/ams.rkf /opt/calc1/adf.rkf /opt/results/output.cml
```

Enclose paths in double quotes if there are whitespaces in paths. You must also have write permission on the *outfile* path folder.

## Legal Stuff

Copyright © 2025 by the ioChem-BD Team

The current rkf2cml files are all licensed under the GNU Lesser General Public License, version 3, please read LICENSE file for more information.

The KFReader library files are licensed under the GNU Lesser General Public License, version 3. Please review SCM original package README.txt file for more information.

The Mini-XML library dependency is licensed under the Apache License Version 2.0 with an optional exception to allow linking against GPL2/LGPL2-only software. See the project homepage files "LICENSE" and "NOTICE" for more information.
