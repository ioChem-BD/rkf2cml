 /***************************************************************************
 JumboCML.h - header file for JumboCML: A CML file creation utility from ADF 
              files in .rkf format.

 Copyright (C) 2022 by Institute of Chemical Reasearch of Catalonia (ICIQ)
 For more info, contact ioChem-BD software team (contact at iochem-bd . com)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation version 3 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 ICIQ owns the intellectual property right for this file and reserves the
 right to distrbute it under a license other than LGPL
 ****************************************************************************/

#ifndef _JCML_H_
#define _JCML_H_

#define JCML_MAX_ATOMS 1000000
#define JCML_MAX_INDENTATION_LEVEL 20
#define JCML_BOHR_TO_ANGSTROM 0.52918
#define JCML_AU_TO_EV 27.211399
#define JCML_AU_TO_CAL_MOL_MINUS_ONE 627509.608
#define JCML_AU_TO_KCAL_MOL_MINUS_ONE 627.509608
#define JCML_NMR_EMPTY_FIELD 9876543210.0

void createCmlFile(char *adfpath, char *amspath, char *outputpath);

#endif