 /***************************************************************************
 JumboHelper.h - header file for JumboHelper: A set of helper functions to
                 manage formula generation, text processing and type conversion.

 Copyright (C) 2022 by Institute of Chemical Reasearch of Catalonia (ICIQ)
 For more info, contact ioChem-BD software team (contact at iochem-bd . com)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation version 3 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 ICIQ owns the intellectual property right for this file and reserves the
 right to distrbute it under a license other than LGPL
 ****************************************************************************/

#ifndef _JHEL_H_
#define _JHEL_H_

// Data manipulation functions
char *convertUnits(char *value, double constant);
int startsWith(char *str, char *substr);
char *toUpper(char *value);
char *trim(char *value);
char *rtrim(char *value);
char *ltrim(char *value);
int toInt(char *value);
double toDouble(char *value);
char *convertUnitsFromArray(char *array, int size, double conversion);

// Formula/atom related functions
char *getElementType(int atomicNumber);

void clearStoichiometry();
void addAtomType(char *);
char *getStoichiometry();

// Solvent related functions
char *getSolventName(char *uppercaseName);
double getSolventRad(char *solvent);
double getSolventEps(char *solvent);

#endif